const express = require('express');
const bodyParser = require("body-parser");
const multer = require("multer");
const fs = require("fs");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const {v4: uuidv4} = require("uuid");
const _ = require('lodash');
const moment = require('moment');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname));
app.use(multer({dest: "uploads"}).single("fileData"));

const path = './db/fake.json';

const saveData = (data) => {
    let stringify = JSON.stringify(data);

    fs.writeFileSync(path, stringify);
}

const getData = () => {
    let json = fs.readFileSync(path);

    return JSON.parse(json);
}

const getUserById = (id) => {
    let data = getData();

    return _.filter(data.users, (user) => {
        return user.id == id;
    });
}

app.post('/users/register', (req, res) => {
    let data = getData();

    let id = uuidv4();
    let password = req.body.password;

    if (password.length >= 6) {
        bcrypt.genSalt(saltRounds, function (err, salt) {
            bcrypt.hash(password, salt, function (err, hash) {
                if (!err) {
                    let newUser = {
                        id: id,
                        "username": req.body.username,
                        "email": req.body.email,
                        "password": hash,
                        "token": ""
                    };

                    data["users"].push(newUser);
                    saveData(data);
                }
            });
        });
        res.send('User created successfully');
    } else {
        res.sendStatus(404);
    }
})

app.post('/users/login', (req, res) => {
    let data = getData();

    let userIndex = '';
    const filtered = _.filter(data.users, (user, index) => {
        userIndex = index;
        return user.email == req.body.email && bcrypt.compareSync(req.body.password, user.password);
    });

    if (!_.isEmpty(filtered)) {

        data['users'][userIndex]['token'] = uuidv4();
        data['users'][userIndex]['token_expired'] = moment().add(1, 'hours');
        saveData(data);
        res.send('User logged in');
    } else {
        res.send('User not found');
    }
})

app.post('/users/upload', (req, res) => {

    let userObj = getUserById(req.body.authorId);

    if (_.isEmpty(userObj)) {
        res.send("User not found");
    }

    if (userObj[0].token == "") {
        res.send("User token expired");
    }

    let fileData = req.file;


    if (fileData) {
        let mimeTypes = ['image/gif', 'image/jpeg', 'image/jpg', 'image/png'];

        if (!_.includes(mimeTypes, fileData.mimetype)) {
            res.status(415).json({error: 'Unsupported media type'});
        }

        let data = getData();

        let newPhoto = {
            id: uuidv4(),
            title: fileData.originalname,
            authorId: req.body.authorId,
            path: fileData.path
        };

        data["photos"].push(newPhoto);
        saveData(data);


        res.send("File uploaded successfully");
    } else {
        res.send("File not uploaded");
    }
})

const minutes = 5, the_interval = 1000;
setInterval(function () {
    let data = getData();

    data['users'] = data['users'].map(item => {
        let temp = Object.assign({}, item);

        if (temp.token != '') {

            let start = moment(temp.token_expired);
            let end = moment();
            let duration = moment.duration(end.diff(start));
            let hours = duration.asHours();

            if (hours >= 1) {
                temp.token = '';
                temp.token_expired = '';
            }
        }

        return temp;
    });

    saveData(data);
}, the_interval);


app.listen(8080, () => {
    console.log("Server started");
})